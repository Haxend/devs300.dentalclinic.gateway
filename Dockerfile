#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM --platform=$BUILDPLATFORM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG TARGETARCH
COPY . /source
WORKDIR /source/Presentation

RUN --mount=type=cache,id=nuget,target=/root/.nuget/packages \
    dotnet publish -a $(echo "$TARGETARCH" | sed 's/amd64/x64/') --use-current-runtime --self-contained false -o /app

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS final
WORKDIR /app
COPY --from=build /app .
USER $APP_UID
ENTRYPOINT ["dotnet", "DentalClinic.Gateway.Presentation.dll"]
