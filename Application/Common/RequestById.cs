﻿using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.Gateway.Application.Common;

public abstract record RequestById
{
    [FromRoute]
    public Guid Id { get; set; }
}