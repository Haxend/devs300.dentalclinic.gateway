﻿using AutoMapper;
using DentalClinic.Common.Contracts.ServicePrice;
using DentalClinic.Common.Contracts.ServicePrice.Responses;
using DentalClinic.Gateway.Application.Features.ServicePriceFeatures.CreateServicePrice;
using DentalClinic.Gateway.Application.Features.ServicePriceFeatures.DeleteServicePrice;
using DentalClinic.Gateway.Application.Features.ServicePriceFeatures.GetServicePrice;
using DentalClinic.Gateway.Application.Features.ServicePriceFeatures.UpdateServicePrice;

namespace DentalClinic.Gateway.Application.Mappings
{
    public class ServicePriceMapper : Profile
    {
        public ServicePriceMapper() 
        {
            // Create
            CreateMap<_CreateServicePriceRequest, ServicePriceCreateRequest>()
                .ForMember(d => d.Code, e => e.MapFrom(s => s.Code))
                .ForMember(d => d.Price, e => e.MapFrom(s => s.Price))
                .ForMember(d => d.Name, e => e.MapFrom(s => s.Name));
            CreateMap<ServicePriceCreateResponse, _CreateServicePriceResponse>();

            // Get
            CreateMap<_GetServicePriceRequest, ServicePriceGetRequest>();
            CreateMap<ServicePriceGetResponse, _GetServicePriceResponse>();

            // Update
            CreateMap<_UpdateServicePriceRequest, ServicePriceUpdateRequest>();
            CreateMap<ServicePriceUpdateResponse, _UpdateServicePriceResponse>();

            // Delete
            CreateMap<_DeleteServicePriceRequest, ServicePriceDeleteRequest>();
            CreateMap<ServicePriceDeleteResponse, _DeleteServicePriceResponse>();
        }
    }
}
