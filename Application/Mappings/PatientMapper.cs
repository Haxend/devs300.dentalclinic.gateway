﻿using AutoMapper;
using DentalClinic.Common;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.DeletePatient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.GetAllPatient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.GetPatient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.PatientClient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.UpdatePatient;

namespace DentalClinic.Gateway.Application.Mappings
{
    public class PatientMapper : Profile
    {
        public PatientMapper()
        {
            // Create
            CreateMap<_CreatePatientRequest, PatientCreateRequest>()
                .ForMember(d => d.RoleId, e => e.MapFrom(s => Roles.GetAllRolesWithIds()[Roles.Client]))
                .ForMember(d => d.Email, e => e.MapFrom(s => s.Email))
                .ForMember(d => d.PasswordHash, e => e.MapFrom(s => s.PasswordHash))
                .ForMember(d => d.LastName, e => e.MapFrom(s => s.LastName))
                .ForMember(d => d.FirstName, e => e.MapFrom(s => s.FirstName))
                .ForMember(d => d.FatherName, e => e.MapFrom(s => s.FatherName))
                .ForMember(d => d.Allergy, e => e.MapFrom(s => s.Allergy))
                .ForMember(d => d.PhoneNumber, e => e.MapFrom(s => s.PhoneNumber));
            CreateMap<PatientCreateResponse, _CreatePatientResponse>();

            // Get
            CreateMap<_GetPatientRequest, PatientGetRequest>();
            CreateMap<PatientGetResponse, _GetPatientResponse>();

            // GetAll
            CreateMap<_GetAllPatientRequest, PatientGetListRequest>();
            CreateMap<PatientGetListResponse, _GetAllPatientResponse>();

            // Update
            CreateMap<_UpdatePatientRequest, PatientUpdateRequest>()
                .ForMember(d => d.RoleId, e => e.MapFrom(s => Roles.GetAllRolesWithIds()[Roles.Admin]))
                .ForMember(d => d.Email, e => e.MapFrom(s => s.Body.Email))
                .ForMember(d => d.LastName, e => e.MapFrom(s => s.Body.LastName))
                .ForMember(d => d.FirstName, e => e.MapFrom(s => s.Body.FirstName))
                .ForMember(d => d.FatherName, e => e.MapFrom(s => s.Body.FatherName))
                .ForMember(d => d.PhoneNumber, e => e.MapFrom(s => s.Body.PhoneNumber))
                .ForMember(d => d.PasswordHash, e => e.MapFrom(s => s.Body.PasswordHash))
                .ForMember(d => d.Allergy, e => e.MapFrom(s => s.Body.Allergy));
            CreateMap<PatientUpdateResponse, _UpdatePatientResponse>();

            // Delete
            CreateMap<_DeletePatientRequest, PatientDeleteRequest>();
            CreateMap<PatientDeleteResponse, _DeletePatientResponse>();
        }
    }
}
