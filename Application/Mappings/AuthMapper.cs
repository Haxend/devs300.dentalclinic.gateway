using AutoMapper;
using DentalClinic.Common.Contracts.Auth.Requests;
using DentalClinic.Common.DentalClinic.Contracts.Auth.Responses;
using DentalClinic.Gateway.Application.Features.AuthFeatures;

namespace DentalClinic.Gateway.Application.Mappings;

public sealed class AuthMapper : Profile
{
    public AuthMapper()
    {
        CreateMap<AuthenticateRequest, Authenticate>();
        CreateMap<AuthenticateResult, AuthenticateResponse>();
    }
}