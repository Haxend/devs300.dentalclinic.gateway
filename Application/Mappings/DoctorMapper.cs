﻿using AutoMapper;
using DentalClinic.Common;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.DeleteDoctor;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.DoctorClient;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.GetAllDoctor;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.GetDoctor;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.UpdateDoctor;

namespace DentalClinic.Gateway.Application.Mappings
{
    public class DoctorMapper : Profile
    {
        public DoctorMapper()
        {
            // Create
            CreateMap<_CreateDoctorRequest, DoctorCreateRequest>()
                .ForMember(d => d.RoleId, e => e.MapFrom(s => Roles.GetAllRolesWithIds()[Roles.Admin]))
                .ForMember(d => d.Email, e => e.MapFrom(s => s.Email))
                .ForMember(d => d.PasswordHash, e => e.MapFrom(s => s.PasswordHash))
                .ForMember(d => d.LastName, e => e.MapFrom(s => s.LastName))
                .ForMember(d => d.FirstName, e => e.MapFrom(s => s.FirstName))
                .ForMember(d => d.FatherName, e => e.MapFrom(s => s.FatherName))
                .ForMember(d => d.Qualification, e => e.MapFrom(s => s.Qualification))
                .ForMember(d => d.PhoneNumber, e => e.MapFrom(s => s.PhoneNumber));
            CreateMap<DoctorCreateResponse, _CreateDoctorResponse>();

            // Get
            CreateMap<_GetDoctorRequest, DoctorGetRequest>();
            CreateMap<DoctorGetResponse, _GetDoctorResponse>();

            // GetAll
            CreateMap<_GetAllDoctorRequest, DoctorGetListRequest>();
            CreateMap<DoctorGetListResponse, _GetAllDoctorResponse>();

            // Update
            CreateMap<_UpdateDoctorRequest, DoctorUpdateRequest>()
                .ForMember(d => d.RoleId, e => e.MapFrom(s => Roles.GetAllRolesWithIds()[Roles.Admin]))
                .ForMember(d => d.Email, e => e.MapFrom(s => s.Body.Email))
                .ForMember(d => d.LastName, e => e.MapFrom(s => s.Body.LastName))
                .ForMember(d => d.FirstName, e => e.MapFrom(s => s.Body.FirstName))
                .ForMember(d => d.FatherName, e => e.MapFrom(s => s.Body.FatherName))
                .ForMember(d => d.PhoneNumber, e => e.MapFrom(s => s.Body.PhoneNumber))
                .ForMember(d => d.PasswordHash, e => e.MapFrom(s => s.Body.PasswordHash))
                .ForMember(d => d.Qualification, e => e.MapFrom(s => s.Body.Qualification));
            CreateMap<DoctorUpdateResponse, _UpdateDoctorResponse>();

            // Delete
            CreateMap<_DeleteDoctorRequest, DoctorDeleteRequest>();
            CreateMap<DoctorDeleteResponse, _DeleteDoctorResponse>();
        }
    }
}
