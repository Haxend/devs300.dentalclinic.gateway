﻿using AutoMapper;
using DentalClinic.Common;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Common.Contracts.Receptionist;
using DentalClinic.Gateway.Application.Features.PatientFeatures.GetAllPatient;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.DeleteReceptionist;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.GetAllReceptionist;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.GetReceptionist;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.ReceptionistClient;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.UpdateReceptionist;

namespace DentalClinic.Gateway.Application.Mappings
{
    public class ReceptionistMapper : Profile
    {
        public ReceptionistMapper() 
        {
            // Create
            CreateMap<_CreateReceptionistRequest, ReceptionistCreateRequest>()
                .ForMember(d => d.RoleId, e => e.MapFrom(s => Roles.GetAllRolesWithIds()[Roles.SuperAdmin]))
                .ForMember(d => d.Email, e => e.MapFrom(s => s.Email))
                .ForMember(d => d.PasswordHash, e => e.MapFrom(s => s.PasswordHash))
                .ForMember(d => d.LastName, e => e.MapFrom(s => s.LastName))
                .ForMember(d => d.FirstName, e => e.MapFrom(s => s.FirstName))
                .ForMember(d => d.FatherName, e => e.MapFrom(s => s.FatherName))
                .ForMember(d => d.PhoneNumber, e => e.MapFrom(s => s.PhoneNumber));
            CreateMap<ReceptionistCreateResponse, _CreateReceptionistResponse>();

            // Get
            CreateMap<_GetReceptionistRequest, ReceptionistGetRequest>();
            CreateMap<ReceptionistGetResponse, _GetReceptionistResponse>();

            // GetAll
            CreateMap<_GetAllReceptionistRequest, ReceptionistGetListRequest>();
            CreateMap<ReceptionistGetListResponse, _GetAllReceptionistResponse>();

            // Update
            CreateMap<_UpdateReceptionistRequest, ReceptionistUpdateRequest>()
                .ForMember(d => d.RoleId, e => e.MapFrom(s => Roles.GetAllRolesWithIds()[Roles.SuperAdmin]))
                .ForMember(d => d.Email, e => e.MapFrom(s => s.Body.Email))
                .ForMember(d => d.LastName, e => e.MapFrom(s => s.Body.LastName))
                .ForMember(d => d.FirstName, e => e.MapFrom(s => s.Body.FirstName))
                .ForMember(d => d.FatherName, e => e.MapFrom(s => s.Body.FatherName))
                .ForMember(d => d.PhoneNumber, e => e.MapFrom(s => s.Body.PhoneNumber))
                .ForMember(d => d.PasswordHash, e => e.MapFrom(s => s.Body.PasswordHash));
            CreateMap<ReceptionistUpdateResponse, _UpdateReceptionistResponse>();

            // Delete
            CreateMap<_DeleteReceptionistRequest, ReceptionistDeleteRequest>();
            CreateMap<ReceptionistDeleteResponse, _DeleteReceptionistResponse>();
        }
    }
}
