namespace DentalClinic.Gateway.Application.Models.Doctor;

public record UpdateDoctorDto : BaseDoctorDto;