namespace DentalClinic.Gateway.Application.Models.Doctor;

public record CreationDoctorDto : BaseDoctorDto;