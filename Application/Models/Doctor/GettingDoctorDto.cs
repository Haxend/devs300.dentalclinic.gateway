﻿namespace DentalClinic.Gateway.Application.Models.Doctor;

public record GettingDoctorDto : BaseDoctorDto
{
    public Guid Id { get; set; }
}