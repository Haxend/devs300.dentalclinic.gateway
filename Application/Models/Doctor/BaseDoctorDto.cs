﻿namespace DentalClinic.Gateway.Application.Models.Doctor;

public abstract record BaseDoctorDto : BaseUserDto
{
    public string? Qualification { get; set; }

}