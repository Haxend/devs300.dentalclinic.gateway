namespace DentalClinic.Gateway.Application.Models;

public abstract record BaseUserDto
{
    public string Email { get; set; }
    public string FirstName { get; set; }
    public string FatherName { get; set; }
    public string LastName { get; set; }
    public string PhoneNumber { get; set; }
    public string? PasswordHash { get; set; }
}