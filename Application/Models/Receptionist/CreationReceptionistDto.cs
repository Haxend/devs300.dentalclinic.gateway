namespace DentalClinic.Gateway.Application.Models.Receptionist;

public record CreationReceptionistDto : BaseReceptionistDto;