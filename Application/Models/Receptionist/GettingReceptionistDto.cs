﻿namespace DentalClinic.Gateway.Application.Models.Receptionist;

public record GettingReceptionistDto : BaseReceptionistDto
{
    public Guid Id { get; set; }
}