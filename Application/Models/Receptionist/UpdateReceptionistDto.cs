namespace DentalClinic.Gateway.Application.Models.Receptionist;

public record UpdateReceptionistDto : BaseReceptionistDto;