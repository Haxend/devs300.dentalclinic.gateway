﻿namespace DentalClinic.Gateway.Application.Models.Receptionist;

public abstract record BaseReceptionistDto : BaseUserDto;