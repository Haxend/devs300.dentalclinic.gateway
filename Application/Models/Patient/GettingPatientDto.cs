﻿namespace DentalClinic.Gateway.Application.Models.Patient;

public record GettingPatientDto : BasePatientDto
{
    public Guid Id { get; set; }
}