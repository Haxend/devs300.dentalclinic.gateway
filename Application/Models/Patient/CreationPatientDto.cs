namespace DentalClinic.Gateway.Application.Models.Patient;

public record CreationPatientDto : BasePatientDto;