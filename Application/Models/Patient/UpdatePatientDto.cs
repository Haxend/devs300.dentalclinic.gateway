namespace DentalClinic.Gateway.Application.Models.Patient;

public record UpdatePatientDto : BasePatientDto;