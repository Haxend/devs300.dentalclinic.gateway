﻿namespace DentalClinic.Gateway.Application.Models.Patient;

public abstract record BasePatientDto : BaseUserDto
{
    public string? Allergy { get; set; }

}