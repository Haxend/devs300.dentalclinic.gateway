﻿namespace DentalClinic.Gateway.Application.Models.ServicePrice
{
    public record CreationServicePriceDto : BaseServicePriceDto;
}
