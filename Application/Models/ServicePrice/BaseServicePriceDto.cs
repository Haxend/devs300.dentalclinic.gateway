﻿namespace DentalClinic.Gateway.Application.Models.ServicePrice
{
    public abstract record BaseServicePriceDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
    }
}
