﻿namespace DentalClinic.Gateway.Application.Models.ServicePrice
{
    public record UpdateServicePriceDto : BaseServicePriceDto;
}
