﻿namespace DentalClinic.Gateway.Application.Models.ServicePrice
{
    public record GettingServicePriceDto : BaseServicePriceDto
    {
        public Guid Id { get; set; }
    }
}
