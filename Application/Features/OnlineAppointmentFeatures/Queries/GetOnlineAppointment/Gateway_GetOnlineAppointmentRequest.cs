﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.GetOnlineAppointment
{
    public class Gateway_GetOnlineAppointmentRequest : IRequest<Gateway_GetOnlineAppointmentResponse>
    {
        [FromRoute]
        public Guid Id { get; set; }
    }
}
