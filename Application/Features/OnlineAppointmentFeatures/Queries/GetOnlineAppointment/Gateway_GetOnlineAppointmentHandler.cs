﻿using AutoMapper;
using DentalClinic.Common.Contracts.OnlineAppointment.Requests;
using DentalClinic.Common.Contracts.OnlineAppointment.Responses;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.GetOnlineAppointment
{
    public class Gateway_GetOnlineAppointmentHandler : IRequestHandler<Gateway_GetOnlineAppointmentRequest, Gateway_GetOnlineAppointmentResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<OnlineAppointmentGetRequest> _requestClient;

        public Gateway_GetOnlineAppointmentHandler(IMapper mapper, IRequestClient<OnlineAppointmentGetRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<Gateway_GetOnlineAppointmentResponse> Handle(Gateway_GetOnlineAppointmentRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<OnlineAppointmentGetResponse>
                (_mapper.Map<OnlineAppointmentGetRequest>(request), cancellationToken);

            return _mapper.Map<Gateway_GetOnlineAppointmentResponse>(response.Message);
        }
    }
}
