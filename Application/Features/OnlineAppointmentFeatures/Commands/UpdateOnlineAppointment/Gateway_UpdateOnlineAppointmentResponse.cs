﻿namespace DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.UpdateOnlineAppointment
{
    public class Gateway_UpdateOnlineAppointmentResponse
    {
        public Guid Id { get; set; }

        public string? PhoneNumber { get; set; }

        public string? Email { get; set; }

        public DateTime? WantedDateVisit { get; set; }

        public string? Description { get; set; }

        public bool Approved { get; set; }
    }
}
