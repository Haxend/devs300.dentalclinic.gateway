﻿using AutoMapper;
using DentalClinic.Common.Contracts.OnlineAppointment.Requests;
using DentalClinic.Common.Contracts.OnlineAppointment.Responses;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.UpdateOnlineAppointment
{
    public class Gateway_UpdateOnlineAppointmentHandler : IRequestHandler<Gateway_UpdateOnlineAppointmentRequest, Gateway_UpdateOnlineAppointmentResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<OnlineAppointmentUpdateRequest> _requestClient;

        public Gateway_UpdateOnlineAppointmentHandler(IMapper mapper, IRequestClient<OnlineAppointmentUpdateRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<Gateway_UpdateOnlineAppointmentResponse> Handle(Gateway_UpdateOnlineAppointmentRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<OnlineAppointmentUpdateResponse>
                (_mapper.Map<OnlineAppointmentUpdateRequest>(request), cancellationToken);

            return _mapper.Map<Gateway_UpdateOnlineAppointmentResponse>(response.Message);
        }
    }
}
