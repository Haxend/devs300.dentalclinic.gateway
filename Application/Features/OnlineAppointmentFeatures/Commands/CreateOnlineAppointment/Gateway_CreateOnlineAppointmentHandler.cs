﻿using AutoMapper;
using DentalClinic.Common.Contracts.OnlineAppointment.Requests;
using DentalClinic.Common.Contracts.OnlineAppointment.Responses;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.CreateOnlineAppointment
{
    public class Gateway_CreateOnlineAppointmentHandler : IRequestHandler<Gateway_CreateOnlineAppointmentRequest, Gateway_CreateOnlineAppointmentResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<OnlineAppointmentCreateRequest> _requestClient;

        public Gateway_CreateOnlineAppointmentHandler(IMapper mapper, IRequestClient<OnlineAppointmentCreateRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<Gateway_CreateOnlineAppointmentResponse> Handle(Gateway_CreateOnlineAppointmentRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<OnlineAppointmentCreateResponse>
                (_mapper.Map<OnlineAppointmentCreateRequest>(request), cancellationToken);

            return _mapper.Map<Gateway_CreateOnlineAppointmentResponse>(response.Message);
        }
    }
}
