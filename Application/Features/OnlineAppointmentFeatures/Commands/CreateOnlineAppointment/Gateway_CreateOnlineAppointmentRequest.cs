﻿using MediatR;

namespace DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.CreateOnlineAppointment
{
    public class Gateway_CreateOnlineAppointmentRequest : IRequest<Gateway_CreateOnlineAppointmentResponse>
    {
        public string? PhoneNumber { get; set; }

        public string? Email { get; set; }

        public DateTime? WantedDateVisit { get; set; }

        public string? Description { get; set; }

        public bool Approved { get; set; }
    }
}
