﻿using AutoMapper;
using DentalClinic.Common.Contracts.OnlineAppointment.Requests;
using DentalClinic.Common.Contracts.OnlineAppointment.Responses;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.DeleteOnlineAppointment
{
    public class Gateway_DeleteOnlineAppointmentHandler : IRequestHandler<Gateway_DeleteOnlineAppointmentRequest, Gateway_DeleteOnlineAppointmentResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<OnlineAppointmentDeleteRequest> _requestClient;

        public Gateway_DeleteOnlineAppointmentHandler(IMapper mapper, IRequestClient<OnlineAppointmentDeleteRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<Gateway_DeleteOnlineAppointmentResponse> Handle(Gateway_DeleteOnlineAppointmentRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<OnlineAppointmentDeleteResponse>
                (_mapper.Map<OnlineAppointmentDeleteRequest>(request), cancellationToken);

            return _mapper.Map<Gateway_DeleteOnlineAppointmentResponse>(response.Message);
        }
    }
}
