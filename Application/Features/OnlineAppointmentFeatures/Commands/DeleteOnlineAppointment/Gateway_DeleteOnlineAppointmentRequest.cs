﻿using MediatR;

namespace DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.DeleteOnlineAppointment
{
    public class Gateway_DeleteOnlineAppointmentRequest : IRequest<Gateway_DeleteOnlineAppointmentResponse>
    {
        public Guid Id { get; set; }
    }
}
