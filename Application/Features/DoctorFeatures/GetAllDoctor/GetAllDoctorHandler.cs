﻿using AutoMapper;
using DentalClinic.Common.Contracts.Doctor;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.GetAllDoctor
{
    public sealed class GetAllDoctorHandler : IRequestHandler<_GetAllDoctorRequest, _GetAllDoctorResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<DoctorGetListRequest> _requestClient;

        public GetAllDoctorHandler(IMapper mapper, IRequestClient<DoctorGetListRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<_GetAllDoctorResponse> Handle(_GetAllDoctorRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<DoctorGetListResponse>
                (_mapper.Map<DoctorGetListRequest>(request), cancellationToken);

            return new _GetAllDoctorResponse
            {
                Offset = request.Offset,
                TotalCount = response.Message.Elements.Count,
                Count = request.Count,
                Items = _mapper.Map<List<GettingDoctorDto>>(response.Message.Elements)
            };
        }
    }
}
