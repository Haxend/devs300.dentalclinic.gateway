﻿using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Gateway.Application.Common;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.GetAllDoctor
{
    public record _GetAllDoctorResponse : ResponseWithPagination<GettingDoctorDto>;
}
