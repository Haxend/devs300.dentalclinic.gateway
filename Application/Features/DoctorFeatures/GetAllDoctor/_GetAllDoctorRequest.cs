﻿using DentalClinic.Gateway.Application.Common;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.GetAllDoctor
{
    public sealed record _GetAllDoctorRequest : RequestWithPagination, IRequest<_GetAllDoctorResponse>;
}
