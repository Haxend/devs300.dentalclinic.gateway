﻿using DentalClinic.Gateway.Application.Models.Doctor;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.DoctorClient;

public sealed record _CreateDoctorRequest : BaseDoctorDto, IRequest<_CreateDoctorResponse>;