﻿using AutoMapper;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.DoctorClient;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.CreateDoctor;

public sealed class CreateDoctorHandler : IRequestHandler<_CreateDoctorRequest, _CreateDoctorResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<DoctorCreateRequest> _requestClient;

    public CreateDoctorHandler(IMapper mapper, IRequestClient<DoctorCreateRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }
    
    public async Task<_CreateDoctorResponse> Handle(_CreateDoctorRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<DoctorCreateResponse>
            (_mapper.Map<DoctorCreateRequest>(request), cancellationToken);
        
        return _mapper.Map<_CreateDoctorResponse>(response.Message);
    }
}