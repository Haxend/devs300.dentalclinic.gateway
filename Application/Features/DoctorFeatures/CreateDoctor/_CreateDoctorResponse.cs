﻿using DentalClinic.Gateway.Application.Models.Doctor;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.DoctorClient;

public sealed record _CreateDoctorResponse : GettingDoctorDto;