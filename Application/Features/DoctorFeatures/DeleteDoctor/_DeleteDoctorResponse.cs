﻿using DentalClinic.Gateway.Application.Models.Common;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.DeleteDoctor;

public sealed record _DeleteDoctorResponse : BaseDeletionResponse;