﻿using AutoMapper;
using DentalClinic.Common.Contracts.Doctor;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.DeleteDoctor;

public sealed class DeleteDoctorHandler : IRequestHandler<_DeleteDoctorRequest, _DeleteDoctorResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<DoctorDeleteRequest> _requestClient;

    public DeleteDoctorHandler(IMapper mapper, IRequestClient<DoctorDeleteRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }

    public async Task<_DeleteDoctorResponse> Handle(_DeleteDoctorRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<DoctorDeleteResponse>
            (_mapper.Map<DoctorDeleteRequest>(request), cancellationToken);
        
        return _mapper.Map<_DeleteDoctorResponse>(response.Message);
    }
}