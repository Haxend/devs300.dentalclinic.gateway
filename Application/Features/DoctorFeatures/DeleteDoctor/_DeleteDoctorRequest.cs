﻿using DentalClinic.Gateway.Application.Common;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.DeleteDoctor;

public sealed record _DeleteDoctorRequest : RequestById, IRequest<_DeleteDoctorResponse>;