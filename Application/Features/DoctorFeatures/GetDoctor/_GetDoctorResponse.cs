﻿using DentalClinic.Gateway.Application.Models.Doctor;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.GetDoctor;

public sealed record _GetDoctorResponse : GettingDoctorDto;