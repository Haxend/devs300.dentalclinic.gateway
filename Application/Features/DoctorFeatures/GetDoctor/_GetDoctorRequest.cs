﻿using DentalClinic.Gateway.Application.Common;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.GetDoctor;

public sealed record _GetDoctorRequest : RequestById, IRequest<_GetDoctorResponse>;