﻿using AutoMapper;
using DentalClinic.Common.Contracts.Doctor;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.GetDoctor;

public sealed class GetDoctorHandler : IRequestHandler<_GetDoctorRequest, _GetDoctorResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<DoctorGetRequest> _requestClient;

    public GetDoctorHandler(IMapper mapper, IRequestClient<DoctorGetRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }

    public async Task<_GetDoctorResponse> Handle(_GetDoctorRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<DoctorGetResponse>
            (_mapper.Map<DoctorGetRequest>(request), cancellationToken);
        
        return _mapper.Map<_GetDoctorResponse>(response.Message);
    }
}