﻿using DentalClinic.Gateway.Application.Models.Doctor;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.UpdateDoctor;

public record _UpdateDoctorResponse : GettingDoctorDto;