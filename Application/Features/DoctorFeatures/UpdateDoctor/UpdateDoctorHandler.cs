﻿using AutoMapper;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.UpdateDoctor;
using MassTransit;
using MediatR;

namespace Booking.Gateway.Application.Features.ClientFeatures.UpdateClient;

public sealed class UpdateDoctorHandler : IRequestHandler<_UpdateDoctorRequest, _UpdateDoctorResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<DoctorUpdateRequest> _requestClient;

    public UpdateDoctorHandler(IMapper mapper, IRequestClient<DoctorUpdateRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }
    
    public async Task<_UpdateDoctorResponse> Handle(_UpdateDoctorRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<DoctorUpdateResponse>
            (_mapper.Map<DoctorUpdateRequest>(request), cancellationToken);
        
        return _mapper.Map<_UpdateDoctorResponse>(response.Message);
    }
}