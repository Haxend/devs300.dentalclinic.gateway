﻿using DentalClinic.Gateway.Application.Models.Doctor;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.Gateway.Application.Features.DoctorFeatures.UpdateDoctor;

public sealed record _UpdateDoctorRequest : IRequest<_UpdateDoctorResponse>
{
    [FromRoute]
    public Guid Id { get; set; }
    [FromBody]
    public UpdateDoctorDto Body { get; set; }
}