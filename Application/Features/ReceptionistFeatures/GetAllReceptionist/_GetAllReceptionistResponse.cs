﻿using DentalClinic.Common.Contracts.Receptionist;
using DentalClinic.Gateway.Application.Common;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.GetAllReceptionist
{
    public record _GetAllReceptionistResponse : ResponseWithPagination<GettingReceptionistDto>;
}
