﻿using AutoMapper;
using DentalClinic.Common.Contracts.Receptionist;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.GetAllReceptionist
{
    public sealed class GetAllReceptionistHandler : IRequestHandler<_GetAllReceptionistRequest, _GetAllReceptionistResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<ReceptionistGetListRequest> _requestClient;

        public GetAllReceptionistHandler(IMapper mapper, IRequestClient<ReceptionistGetListRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<_GetAllReceptionistResponse> Handle(_GetAllReceptionistRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<ReceptionistGetListResponse>
                (_mapper.Map<ReceptionistGetListRequest>(request), cancellationToken);

            return new _GetAllReceptionistResponse
            {
                Offset = request.Offset,
                TotalCount = response.Message.Elements.Count,
                Count = request.Count,
                Items = _mapper.Map<List<GettingReceptionistDto>>(response.Message.Elements)
            };
        }
    }
}
