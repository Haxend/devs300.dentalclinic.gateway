﻿using DentalClinic.Gateway.Application.Common;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.GetAllReceptionist
{
    public sealed record _GetAllReceptionistRequest : RequestWithPagination, IRequest<_GetAllReceptionistResponse>;
}
