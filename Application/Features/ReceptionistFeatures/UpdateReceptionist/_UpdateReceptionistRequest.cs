﻿using DentalClinic.Gateway.Application.Models.Receptionist;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.UpdateReceptionist;

public sealed record _UpdateReceptionistRequest : IRequest<_UpdateReceptionistResponse>
{
    [FromRoute]
    public Guid Id { get; set; }
    [FromBody]
    public UpdateReceptionistDto Body { get; set; }
}