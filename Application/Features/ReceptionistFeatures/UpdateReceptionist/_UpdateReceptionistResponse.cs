﻿using DentalClinic.Gateway.Application.Models.Receptionist;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.UpdateReceptionist;

public record _UpdateReceptionistResponse : GettingReceptionistDto;