﻿using AutoMapper;
using DentalClinic.Common.Contracts.Receptionist;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.UpdateReceptionist;
using MassTransit;
using MediatR;

namespace Booking.Gateway.Application.Features.ReceptionistFeatures.UpdateReceptionist;

public sealed class UpdateReceptionistHandler : IRequestHandler<_UpdateReceptionistRequest, _UpdateReceptionistResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<ReceptionistUpdateRequest> _requestClient;

    public UpdateReceptionistHandler(IMapper mapper, IRequestClient<ReceptionistUpdateRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }
    
    public async Task<_UpdateReceptionistResponse> Handle(_UpdateReceptionistRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<ReceptionistUpdateResponse>
            (_mapper.Map<ReceptionistUpdateRequest>(request), cancellationToken);
        
        return _mapper.Map<_UpdateReceptionistResponse>(response.Message);
    }
}