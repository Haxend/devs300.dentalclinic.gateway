﻿using DentalClinic.Gateway.Application.Common;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.GetReceptionist;

public sealed record _GetReceptionistRequest : RequestById, IRequest<_GetReceptionistResponse>;