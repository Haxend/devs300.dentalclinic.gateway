﻿using DentalClinic.Gateway.Application.Models.Receptionist;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.GetReceptionist;

public sealed record _GetReceptionistResponse : GettingReceptionistDto;