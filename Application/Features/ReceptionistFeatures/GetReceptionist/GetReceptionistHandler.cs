﻿using AutoMapper;
using DentalClinic.Common.Contracts.Receptionist;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.GetReceptionist;

public sealed class GetReceptionistHandler : IRequestHandler<_GetReceptionistRequest, _GetReceptionistResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<ReceptionistGetRequest> _requestClient;

    public GetReceptionistHandler(IMapper mapper, IRequestClient<ReceptionistGetRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }

    public async Task<_GetReceptionistResponse> Handle(_GetReceptionistRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<ReceptionistGetResponse>
            (_mapper.Map<ReceptionistGetRequest>(request), cancellationToken);
        
        return _mapper.Map<_GetReceptionistResponse>(response.Message);
    }
}