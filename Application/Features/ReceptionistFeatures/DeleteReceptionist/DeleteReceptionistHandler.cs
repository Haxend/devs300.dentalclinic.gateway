﻿using AutoMapper;
using DentalClinic.Common.Contracts.Receptionist;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.DeleteReceptionist;

public sealed class DeleteReceptionistHandler : IRequestHandler<_DeleteReceptionistRequest, _DeleteReceptionistResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<ReceptionistDeleteRequest> _requestClient;

    public DeleteReceptionistHandler(IMapper mapper, IRequestClient<ReceptionistDeleteRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }

    public async Task<_DeleteReceptionistResponse> Handle(_DeleteReceptionistRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<ReceptionistDeleteResponse>
            (_mapper.Map<ReceptionistDeleteRequest>(request), cancellationToken);
        
        return _mapper.Map<_DeleteReceptionistResponse>(response.Message);
    }
}