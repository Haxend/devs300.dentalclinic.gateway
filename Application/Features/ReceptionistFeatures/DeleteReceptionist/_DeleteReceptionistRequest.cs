﻿using DentalClinic.Gateway.Application.Common;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.DeleteDoctor;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.DeleteReceptionist;

public sealed record _DeleteReceptionistRequest : RequestById, IRequest<_DeleteReceptionistResponse>;