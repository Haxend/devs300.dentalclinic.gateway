﻿using DentalClinic.Gateway.Application.Models.Common;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.DeleteReceptionist;

public sealed record _DeleteReceptionistResponse : BaseDeletionResponse;