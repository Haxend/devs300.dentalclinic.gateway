﻿using AutoMapper;
using DentalClinic.Common.Contracts.Receptionist;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.ReceptionistClient;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.CreateReceptionist;

public sealed class CreateReceptionistHandler : IRequestHandler<_CreateReceptionistRequest, _CreateReceptionistResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<ReceptionistCreateRequest> _requestClient;

    public CreateReceptionistHandler(IMapper mapper, IRequestClient<ReceptionistCreateRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }
    
    public async Task<_CreateReceptionistResponse> Handle(_CreateReceptionistRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<ReceptionistCreateResponse>
            (_mapper.Map<ReceptionistCreateRequest>(request), cancellationToken);
        
        return _mapper.Map<_CreateReceptionistResponse>(response.Message);
    }
}