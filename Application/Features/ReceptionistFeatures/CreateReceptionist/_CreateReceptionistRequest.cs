﻿using DentalClinic.Gateway.Application.Models.Receptionist;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.ReceptionistClient;

public sealed record _CreateReceptionistRequest : BaseReceptionistDto, IRequest<_CreateReceptionistResponse>;