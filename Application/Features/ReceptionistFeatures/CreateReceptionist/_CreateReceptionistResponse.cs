﻿using DentalClinic.Gateway.Application.Models.Receptionist;

namespace DentalClinic.Gateway.Application.Features.ReceptionistFeatures.ReceptionistClient;

public sealed record _CreateReceptionistResponse : GettingReceptionistDto;