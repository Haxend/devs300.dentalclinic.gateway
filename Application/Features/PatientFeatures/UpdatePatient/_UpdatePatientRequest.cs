﻿using DentalClinic.Gateway.Application.Models.Patient;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.UpdatePatient;

public sealed record _UpdatePatientRequest : IRequest<_UpdatePatientResponse>
{
    [FromRoute]
    public Guid Id { get; set; }
    [FromBody]
    public UpdatePatientDto Body { get; set; }
}