﻿using DentalClinic.Gateway.Application.Models.Patient;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.UpdatePatient;

public record _UpdatePatientResponse : GettingPatientDto;