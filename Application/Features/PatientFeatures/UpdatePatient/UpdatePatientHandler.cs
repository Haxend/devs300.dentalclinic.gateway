﻿using AutoMapper;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.UpdatePatient;
using MassTransit;
using MediatR;

namespace Booking.Gateway.Application.Features.PatientFeatures.UpdatePatient;

public sealed class UpdatePatientHandler : IRequestHandler<_UpdatePatientRequest, _UpdatePatientResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<PatientUpdateRequest> _requestClient;

    public UpdatePatientHandler(IMapper mapper, IRequestClient<PatientUpdateRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }
    
    public async Task<_UpdatePatientResponse> Handle(_UpdatePatientRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<DoctorUpdateResponse>
            (_mapper.Map<PatientUpdateRequest>(request), cancellationToken);
        
        return _mapper.Map<_UpdatePatientResponse>(response.Message);
    }
}