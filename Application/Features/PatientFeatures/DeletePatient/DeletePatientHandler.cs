﻿using AutoMapper;
using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.DeletePatient;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.PatientFeatures.DeletePatient;

public sealed class DeletePatientHandler : IRequestHandler<_DeletePatientRequest, _DeletePatientResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<PatientDeleteRequest> _requestClient;

    public DeletePatientHandler(IMapper mapper, IRequestClient<PatientDeleteRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }

    public async Task<_DeletePatientResponse> Handle(_DeletePatientRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<PatientDeleteResponse>
            (_mapper.Map<PatientDeleteRequest>(request), cancellationToken);
        
        return _mapper.Map<_DeletePatientResponse>(response.Message);
    }
}