﻿using DentalClinic.Gateway.Application.Models.Common;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.DeletePatient;

public sealed record _DeletePatientResponse : BaseDeletionResponse;