﻿using DentalClinic.Gateway.Application.Common;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.DeletePatient;

public sealed record _DeletePatientRequest : RequestById, IRequest<_DeletePatientResponse>;