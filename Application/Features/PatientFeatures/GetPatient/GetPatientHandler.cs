﻿using AutoMapper;
using DentalClinic.Common.Contracts.Patient;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.GetPatient;

public sealed class GetPatientHandler : IRequestHandler<_GetPatientRequest, _GetPatientResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<PatientGetRequest> _requestClient;

    public GetPatientHandler(IMapper mapper, IRequestClient<PatientGetRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }

    public async Task<_GetPatientResponse> Handle(_GetPatientRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<PatientGetResponse>
            (_mapper.Map<PatientGetRequest>(request), cancellationToken);
        
        return _mapper.Map<_GetPatientResponse>(response.Message);
    }
}