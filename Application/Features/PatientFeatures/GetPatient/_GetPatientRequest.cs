﻿using DentalClinic.Gateway.Application.Common;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.GetPatient;

public sealed record _GetPatientRequest : RequestById, IRequest<_GetPatientResponse>;