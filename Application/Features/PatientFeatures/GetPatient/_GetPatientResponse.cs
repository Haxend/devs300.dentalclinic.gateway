﻿using DentalClinic.Gateway.Application.Models.Patient;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.GetPatient;

public sealed record _GetPatientResponse : GettingPatientDto;