﻿using AutoMapper;
using DentalClinic.Common.Contracts.Patient;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.GetAllPatient
{
    public sealed class GetAllPatientHandler : IRequestHandler<_GetAllPatientRequest, _GetAllPatientResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<PatientGetListRequest> _requestClient;

        public GetAllPatientHandler(IMapper mapper, IRequestClient<PatientGetListRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<_GetAllPatientResponse> Handle(_GetAllPatientRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<PatientGetListResponse>
                (_mapper.Map<PatientGetListRequest>(request), cancellationToken);

            return new _GetAllPatientResponse
            {
                Offset = request.Offset,
                TotalCount = response.Message.Elements.Count,
                Count = request.Count,
                Items = _mapper.Map<List<GettingPatientDto>>(response.Message.Elements)
            };
        }
    }
}
