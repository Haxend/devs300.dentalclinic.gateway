﻿using DentalClinic.Gateway.Application.Common;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.GetAllPatient
{
    public sealed record _GetAllPatientRequest : RequestWithPagination, IRequest<_GetAllPatientResponse>;
}
