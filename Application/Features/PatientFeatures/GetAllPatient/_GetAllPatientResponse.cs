﻿using DentalClinic.Common.Contracts.Patient;
using DentalClinic.Gateway.Application.Common;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.GetAllPatient
{
    public record _GetAllPatientResponse : ResponseWithPagination<GettingPatientDto>;
}
