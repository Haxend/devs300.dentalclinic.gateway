﻿using AutoMapper;
using DentalClinic.Common.Contracts.Patient;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.PatientClient;

public sealed class CreatePatientHandler : IRequestHandler<_CreatePatientRequest, _CreatePatientResponse>
{
    private readonly IMapper _mapper;
    private readonly IRequestClient<PatientCreateRequest> _requestClient;

    public CreatePatientHandler(IMapper mapper, IRequestClient<PatientCreateRequest> requestClient)
    {
        _mapper = mapper;
        _requestClient = requestClient;
    }
    
    public async Task<_CreatePatientResponse> Handle(_CreatePatientRequest request, CancellationToken cancellationToken)
    {
        var response = await _requestClient.GetResponse<PatientCreateResponse>
            (_mapper.Map<PatientCreateRequest>(request), cancellationToken);
        
        return _mapper.Map<_CreatePatientResponse>(response.Message);
    }
}