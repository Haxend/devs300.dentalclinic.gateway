﻿using DentalClinic.Gateway.Application.Models.Patient;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.PatientClient;

public sealed record _CreatePatientResponse : GettingPatientDto;