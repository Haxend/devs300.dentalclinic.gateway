﻿using DentalClinic.Gateway.Application.Models.Patient;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.PatientFeatures.PatientClient;

public sealed record _CreatePatientRequest : BasePatientDto, IRequest<_CreatePatientResponse>
{
    public string PasswordHash { get; set; }
}