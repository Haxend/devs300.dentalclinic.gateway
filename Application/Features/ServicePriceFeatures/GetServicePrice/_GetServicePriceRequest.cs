﻿using MediatR;
using RequestById = DentalClinic.Gateway.Application.Common.RequestById;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.GetServicePrice
{
    public sealed record _GetServicePriceRequest : RequestById, IRequest<_GetServicePriceResponse>;
}
