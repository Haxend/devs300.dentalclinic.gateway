﻿using DentalClinic.Gateway.Application.Models.ServicePrice;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.GetServicePrice
{
    public sealed record _GetServicePriceResponse : GettingServicePriceDto;
}
