﻿using AutoMapper;
using DentalClinic.Common.Contracts.ServicePrice;
using DentalClinic.Common.Contracts.ServicePrice.Responses;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.GetServicePrice
{
    public sealed class GetServicePriceHandler : IRequestHandler<_GetServicePriceRequest, _GetServicePriceResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<ServicePriceGetRequest> _requestClient;

        public GetServicePriceHandler(IMapper mapper, IRequestClient<ServicePriceGetRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<_GetServicePriceResponse> Handle(_GetServicePriceRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<ServicePriceGetResponse>
            (_mapper.Map<ServicePriceGetRequest>(request), cancellationToken);

            return _mapper.Map<_GetServicePriceResponse>(response.Message);
        }
    }
}
