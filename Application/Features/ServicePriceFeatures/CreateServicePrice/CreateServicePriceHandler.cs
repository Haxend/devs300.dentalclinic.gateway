﻿using AutoMapper;
using DentalClinic.Common.Contracts.ServicePrice;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.CreateServicePrice
{
    public sealed class CreateServicePriceHandler : IRequestHandler<_CreateServicePriceRequest, _CreateServicePriceResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<ServicePriceCreateRequest> _requestClient;

        public CreateServicePriceHandler(IMapper mapper, IRequestClient<ServicePriceCreateRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<_CreateServicePriceResponse> Handle(_CreateServicePriceRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<ServicePriceCreateResponse>
            (_mapper.Map<ServicePriceCreateRequest>(request), cancellationToken);

            return _mapper.Map<_CreateServicePriceResponse>(response.Message);
        }
    }
}
