﻿using DentalClinic.Gateway.Application.Models.ServicePrice;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.CreateServicePrice
{
    public sealed record _CreateServicePriceRequest : BaseServicePriceDto, IRequest<_CreateServicePriceResponse>;
}
