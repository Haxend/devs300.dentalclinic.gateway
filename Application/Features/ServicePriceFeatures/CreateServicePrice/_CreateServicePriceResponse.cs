﻿using DentalClinic.Gateway.Application.Models.ServicePrice;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.CreateServicePrice
{
    public sealed record _CreateServicePriceResponse : BaseServicePriceDto;
}
