﻿using DentalClinic.Gateway.Application.Models.Common;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.DeleteServicePrice
{
    public sealed record _DeleteServicePriceResponse : BaseDeletionResponse;
}
