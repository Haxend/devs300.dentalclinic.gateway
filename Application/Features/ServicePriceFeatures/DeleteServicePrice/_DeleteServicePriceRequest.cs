﻿using DentalClinic.Common.Contracts.Common;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.DeleteServicePrice
{
    public class _DeleteServicePriceRequest : RequestById, IRequest<_DeleteServicePriceResponse>;
}
