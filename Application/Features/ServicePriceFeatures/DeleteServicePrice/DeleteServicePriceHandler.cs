﻿using AutoMapper;
using DentalClinic.Common.Contracts.Doctor;
using DentalClinic.Common.Contracts.ServicePrice;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.DeleteServicePrice
{
    public sealed class DeleteServicePriceHandler : IRequestHandler<_DeleteServicePriceRequest, _DeleteServicePriceResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<DoctorDeleteRequest> _requestClient;

        public DeleteServicePriceHandler(IMapper mapper, IRequestClient<DoctorDeleteRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<_DeleteServicePriceResponse> Handle(_DeleteServicePriceRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<ServicePriceDeleteResponse>
            (_mapper.Map<ServicePriceDeleteRequest>(request), cancellationToken);

            return _mapper.Map<_DeleteServicePriceResponse>(response.Message);
        }
    }
}
