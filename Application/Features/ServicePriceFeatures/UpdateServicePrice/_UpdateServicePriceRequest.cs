﻿using DentalClinic.Gateway.Application.Models.ServicePrice;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.UpdateServicePrice
{
    public sealed record _UpdateServicePriceRequest : IRequest<_UpdateServicePriceResponse>
    {
        [FromRoute]
        public Guid Id { get; set; }
        [FromBody]
        public UpdateServicePriceDto Body { get; set; }
    }
}
