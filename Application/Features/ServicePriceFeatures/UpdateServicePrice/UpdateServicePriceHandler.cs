﻿using AutoMapper;
using DentalClinic.Common.Contracts.ServicePrice;
using MassTransit;
using MediatR;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.UpdateServicePrice
{
    public sealed class UpdateServicePriceHandler : IRequestHandler<_UpdateServicePriceRequest, _UpdateServicePriceResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRequestClient<ServicePriceUpdateRequest> _requestClient;

        public UpdateServicePriceHandler(IMapper mapper, IRequestClient<ServicePriceUpdateRequest> requestClient)
        {
            _mapper = mapper;
            _requestClient = requestClient;
        }

        public async Task<_UpdateServicePriceResponse> Handle(_UpdateServicePriceRequest request, CancellationToken cancellationToken)
        {
            var response = await _requestClient.GetResponse<ServicePriceUpdateResponse>
            (_mapper.Map<ServicePriceUpdateRequest>(request), cancellationToken);

            return _mapper.Map<_UpdateServicePriceResponse>(response.Message);
        }
    }
}
