﻿using DentalClinic.Gateway.Application.Models.ServicePrice;

namespace DentalClinic.Gateway.Application.Features.ServicePriceFeatures.UpdateServicePrice
{
    public record _UpdateServicePriceResponse : GettingServicePriceDto;
}
