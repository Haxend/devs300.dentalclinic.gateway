using System.IdentityModel.Tokens.Jwt;

namespace DentalClinic.Gateway.Application.Helpers;

public static class JwtTokenHelper
{
    public static Guid GetIdFromToken(string jwtToken)
    {
        var handler = new JwtSecurityTokenHandler();
        var parsedJwtToken = handler.ReadJwtToken(jwtToken);
        return Guid.Parse(parsedJwtToken.Claims.ToList().FirstOrDefault(e => e.Type == "id")?.Value.ToString()!);
    }
}