﻿using DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.CreateOnlineAppointment;
using DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.DeleteOnlineAppointment;
using DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.GetOnlineAppointment;
using DentalClinic.Gateway.Application.Features.OnlineAppointmentFeatures.Commands.UpdateOnlineAppointment;

using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.WebAPI.Controllers;

[ApiController]
[Route("api/onlineappointments")]
public class OnlineAppointmentController : ControllerBase
{
    private readonly IMediator _mediator;

    public OnlineAppointmentController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<Gateway_CreateOnlineAppointmentResponse> Create(Gateway_CreateOnlineAppointmentRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpGet("{Id}")]
    public async Task<Gateway_GetOnlineAppointmentResponse> Get(Gateway_GetOnlineAppointmentRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpPut("{Id}")]
    public async Task<Gateway_UpdateOnlineAppointmentResponse> Update(Gateway_UpdateOnlineAppointmentRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpDelete("{Id}")]
    public async Task<Gateway_DeleteOnlineAppointmentResponse> Delete(Gateway_DeleteOnlineAppointmentRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }
}