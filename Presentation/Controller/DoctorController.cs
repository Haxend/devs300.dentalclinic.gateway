﻿using DentalClinic.Gateway.Application.Features.DoctorFeatures.DeleteDoctor;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.DoctorClient;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.GetAllDoctor;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.GetDoctor;
using DentalClinic.Gateway.Application.Features.DoctorFeatures.UpdateDoctor;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.WebAPI.Controllers;

[ApiController]
[Route("api/doctors")]
public class DoctorController : ControllerBase
{
    private readonly IMediator _mediator;

    public DoctorController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    [HttpPost]
    public async Task<_CreateDoctorResponse> CreateDoctor(_CreateDoctorRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpGet("{Id}")]
    public async Task<_GetDoctorResponse> GetDoctor(_GetDoctorRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpGet]
    public async Task<_GetAllDoctorResponse> GetDoctors([FromQuery] _GetAllDoctorRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpPut("{Id}")]
    public async Task<_UpdateDoctorResponse> UpdateDoctor(_UpdateDoctorRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }
    
    [HttpDelete("{Id}")]
    public async Task<_DeleteDoctorResponse> DeleteDoctor(_DeleteDoctorRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }
}