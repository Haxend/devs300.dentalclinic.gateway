﻿using DentalClinic.Gateway.Application.Features.ServicePriceFeatures.CreateServicePrice;
using DentalClinic.Gateway.Application.Features.ServicePriceFeatures.DeleteServicePrice;
using DentalClinic.Gateway.Application.Features.ServicePriceFeatures.GetServicePrice;
using DentalClinic.Gateway.Application.Features.ServicePriceFeatures.UpdateServicePrice;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.Gateway.Presentation.Controller
{
    [ApiController]
    [Route("api/serviceprices")]
    public class ServicePriceController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ServicePriceController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<_CreateServicePriceResponse> CreateServicePrice(_CreateServicePriceRequest request, CancellationToken cancellationToken)
        {
            return await _mediator.Send(request, cancellationToken);
        }

        [HttpGet("{Id}")]
        public async Task<_GetServicePriceResponse> GetServicePrice(_GetServicePriceRequest request, CancellationToken cancellationToken)
        {
            return await _mediator.Send(request, cancellationToken);
        }

        //[HttpGet]
        //public async Task<_GetAllServicePriceResponse> GetServicePrices([FromQuery] _GetAllServicePriceRequest request, CancellationToken cancellationToken)
        //{
        //    return await _mediator.Send(request, cancellationToken);
        //}

        [HttpPut("{Id}")]
        public async Task<_UpdateServicePriceResponse> UpdateServicePrice(_UpdateServicePriceRequest request, CancellationToken cancellationToken)
        {
            return await _mediator.Send(request, cancellationToken);
        }

        [HttpDelete("{Id}")]
        public async Task<_DeleteServicePriceResponse> DeleteServicePrice(_DeleteServicePriceRequest request, CancellationToken cancellationToken)
        {
            return await _mediator.Send(request, cancellationToken);
        }
    }
}
