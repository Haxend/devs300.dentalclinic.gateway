﻿using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.DeleteReceptionist;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.GetAllReceptionist;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.GetReceptionist;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.ReceptionistClient;
using DentalClinic.Gateway.Application.Features.ReceptionistFeatures.UpdateReceptionist;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.WebAPI.Controllers;

[ApiController]
[Route("api/receptionists")]
public class ReceptionistController : ControllerBase
{
    private readonly IMediator _mediator;

    public ReceptionistController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<_CreateReceptionistResponse> CreateReceptionist(_CreateReceptionistRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpGet("{Id}")]
    public async Task<_GetReceptionistResponse> GetReceptionist(_GetReceptionistRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpGet]
    public async Task<_GetAllReceptionistResponse> GetReceptionists([FromQuery] _GetAllReceptionistRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpPut("{Id}")]
    public async Task<_UpdateReceptionistResponse> UpdateReceptionist(_UpdateReceptionistRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpDelete("{Id}")]
    public async Task<_DeleteReceptionistResponse> DeleteReceptionist(_DeleteReceptionistRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }
}