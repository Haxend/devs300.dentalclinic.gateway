﻿using DentalClinic.Gateway.Application.Features.PatientFeatures.DeletePatient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.GetAllPatient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.GetPatient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.PatientClient;
using DentalClinic.Gateway.Application.Features.PatientFeatures.UpdatePatient;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.WebAPI.Controllers;

[ApiController]
[Route("api/patients")]
public class PatientController : ControllerBase
{
    private readonly IMediator _mediator;

    public PatientController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost]
    public async Task<_CreatePatientResponse> CreatePatient(_CreatePatientRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpGet("{Id}")]
    public async Task<_GetPatientResponse> GetPatient(_GetPatientRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpGet]
    public async Task<_GetAllPatientResponse> GetPatients([FromQuery] _GetAllPatientRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpPut("{Id}")]
    public async Task<_UpdatePatientResponse> UpdatePatient(_UpdatePatientRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [HttpDelete("{Id}")]
    public async Task<_DeletePatientResponse> DeletePatient(_DeletePatientRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }
}