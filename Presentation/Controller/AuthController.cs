using DentalClinic.Gateway.Application.Features.AuthFeatures;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DentalClinic.WebAPI.Controllers;

[ApiController]
[Route("auth")]
public class AuthController : ControllerBase
{
    private readonly IMediator _mediator;

    public AuthController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost("Login")]
    [AllowAnonymous]
    public async Task<AuthenticateResponse> Login(AuthenticateRequest request, CancellationToken cancellationToken)
    {
        return await _mediator.Send(request, cancellationToken);
    }

    [Authorize]
    [HttpGet("check")]
    public async Task<ActionResult<int>> CheckAuth()
    {
        return Ok(1);
    }

}